package br.com.itau;

import java.util.Scanner;

public class IO {

    public static String mensagemInicial() {
        Scanner teclado = new Scanner(System.in);
        System.out.print("Seja bem-vindo ao Blackjack!\nEntre com seu nome para inciar: ");
        return teclado.nextLine();
    }

    public static int apresentarMenu(String nome) {
        Scanner teclado = new Scanner(System.in);
        System.out.print("\n*** Menu ***\n"
                + Menu.INICIAR_JOGO.valor + " - Iniciar jogo\n"
                + Menu.TROCAR_JOGADOR.valor + " - Trocar jogador\n"
                + Menu.RESETAR_PONTUACAO.valor + " - Resetar histórico de pontuações\n"
                + Menu.SAIR.valor + " - Sair\n"
                + "Jogador: " + nome
                + "\nOpção: ");
        return teclado.nextInt();
    }

    public static void melhorPontuacao(String jogador_atual, int pontuacao_jogador_atual,
                                       String maior_pontuador, int maior_pontuacao) {
        if (maior_pontuacao != 0) {
            System.out.println("A maior pontuação alcançada no jogo foi de "
                    + maior_pontuador + " com " + maior_pontuacao + " pontos.");
            System.out.println("Jogador atual: " + jogador_atual + " | Melhor desempenho pessoal: "
                    + pontuacao_jogador_atual + " pontos.");
        }

    }

    public static void opcaoInvalida() {
        System.out.println("Opção Inválida!");
    }

    public static String novoJogador() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Entre com o nome do novo jogador:");
        return teclado.nextLine();
    }

    public static void apresentarCartaSorteada(Carta carta, int soma) {
        System.out.println("Carta sorteada: " + carta.toString());
        System.out.println("Pontuação: " + soma);

    }

    public static boolean continuar() {
        Scanner teclado = new Scanner(System.in);
        while (true) {
            System.out.print("Deseja uma nova carta ?\n"
                    + Menu.SIM.valor + " - Sim\n"
                    + Menu.NAO.valor + " - Não\n"
                    + "Opção: ");
            int opcao = teclado.nextInt();
            if (opcao == Menu.SIM.valor) {
                return true;
            } else if (opcao == Menu.NAO.valor) {
                return false;
            } else {
                opcaoInvalida();
            }
        }
    }

    public static void mensagemDerrota() {
        System.out.println("Infelizmente você ultrapassou 21 pontos.\n"
                + "Tente novamente iniciando um novo jogo.");
    }

    public static void mensagemVitoria() {
        System.out.println("Parabéns! Você venceu ao atingir 21 pontos!!!");
    }

    public static void desistencia(int soma){
        System.out.println("\nVocê finalizou o jogo e fez um total de " + soma + " pontos.\n");
    }

    public static void historicoResetado(){
        System.out.println("Histórico resetado com sucesso!");
    }
}
