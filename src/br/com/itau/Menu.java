package br.com.itau;

public enum Menu {

    INICIAR_JOGO(1), TROCAR_JOGADOR(2), RESETAR_PONTUACAO(3), SAIR(4), SIM(1), NAO(2);

    protected int valor;

    Menu(int valor){
        this.valor = valor;
    }

    public static void iniciarSistema(Jogo jogo){
        while (true) {
            if(jogo.getMelhorPontuacao() != 0){
                IO.melhorPontuacao(jogo.getJogador().getNome(), jogo.getJogador().getMelhorPontuacao(),
                        jogo.getMaiorPontuador(), jogo.getMelhorPontuacao());
            }
            int modo = IO.apresentarMenu(jogo.getJogador().getNome());

            if (modo == Menu.INICIAR_JOGO.valor) {
                jogo.iniciar();

            } else if (modo == Menu.TROCAR_JOGADOR.valor) {
                jogo.setJogador(new Jogador(IO.novoJogador()));

            } else if (modo == Menu.RESETAR_PONTUACAO.valor) {
                jogo.setMelhorPontuacao(0);
                jogo.setMaiorPontuador("");
                jogo.getJogador().setMelhorPontuacao(0);
                IO.historicoResetado();

            } else if (modo == Menu.SAIR.valor) {
                break;

            } else {
                IO.opcaoInvalida();

            }
        }
    }

}


