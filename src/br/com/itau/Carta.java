package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Carta {

    public enum Nome {
        AS(1), DOIS(2), TRES(3), QUATRO(4), CINCO(5), SEIS(6), SETE(7),
        OITO(8), NOVE(9), DEZ(10), DAMA(10), VALETE(10), REI(10);

        private int pontos;

        Nome(int pontos){
            this.pontos = pontos;
        }
    }

    public enum Naipe {
        OURO, ESPADAS, COPAS, PAUS;
    }

    private final Nome nome;
    private final Naipe naipe;
    private final int pontos;

    private Carta(Nome nome, Naipe naipe) {
        this.nome = nome;
        this.naipe = naipe;
        this.pontos = nome.pontos;
    }

    private static final List<Carta> baralho = new ArrayList<Carta>();
    static {
        for(Nome nome : Nome.values()) {
            for(Naipe naipe : Naipe.values()){
                baralho.add(new Carta(nome, naipe));
            }
        }
    }

    public String toString() {
        return nome + " de " + naipe;
    }

    public static ArrayList<Carta> novoBaralho() {
        return new ArrayList<Carta>(baralho);
    }

    public int getPontos() {
        return pontos;
    }
}
