package br.com.itau;

import java.util.Collections;
import java.util.List;

public class Jogo {

    Jogador jogador;
    List<Carta> baralho;
    int melhorPontuacao;
    String maiorPontuador;

    public Jogo(Jogador jogador) {
        this.jogador = jogador;
        this.baralho = Carta.novoBaralho();
        this.melhorPontuacao = 0;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public List<Carta> getBaralho() {
        return baralho;
    }

    public void setBaralho(List<Carta> baralho) {
        this.baralho = baralho;
    }

    public int getMelhorPontuacao() {
        return melhorPontuacao;
    }

    public void setMelhorPontuacao(int melhorPontuacao) {
        this.melhorPontuacao = melhorPontuacao;
    }

    public String getMaiorPontuador() {
        return maiorPontuador;
    }

    public void setMaiorPontuador(String maiorPontuador) {
        this.maiorPontuador = maiorPontuador;
    }

    public void iniciar() {
        novaRodada();
    }

    public void novaRodada() {
        Collections.shuffle(baralho);
        int pontos = 0;

        for (Carta carta : baralho) {
            pontos += carta.getPontos();
            IO.apresentarCartaSorteada(carta, pontos);
            if(pontos > 21){
                IO.mensagemDerrota();
                break;
            }
            else if(pontos == 21){
                IO.mensagemVitoria();
                break;
            }
            else if(!IO.continuar()){
                IO.desistencia(pontos);
                break;
            }
        }
        if(pontos <= 21) {
            jogador.atualizaHistorico(pontos);
            atualizaHistoricoJogo(pontos);
        }

    }

    public void atualizaHistoricoJogo(int pontos) {
        if (getMelhorPontuacao() < pontos) {
            setMelhorPontuacao(pontos);
            setMaiorPontuador(jogador.getNome());
        }
    }

}
