package br.com.itau;

public class Jogador {

    private String nome;
    private int melhorPontuacao;

    public Jogador(String nome){
        this.nome = nome;
        this.melhorPontuacao = 0;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMelhorPontuacao() {
        return melhorPontuacao;
    }

    public void setMelhorPontuacao(int melhorPontuacao) {
        this.melhorPontuacao = melhorPontuacao;
    }

    public void atualizaHistorico(int pontos) {
        if(getMelhorPontuacao() < pontos) {
            setMelhorPontuacao(pontos);
        }
    }

}
